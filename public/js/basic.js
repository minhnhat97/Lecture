$(document).ready(function () {
    // ----------------------------Control for Faculty
    // ---------------------delete faculties
    $('body').on('click', '.btn-delete-item', function () {
        var modal = $('#modalDelete');

        modal.find('.form-delete').attr('action', $(this).data('url'));
        modal.find('.faculty-name').html($(this).data('name'));

        modal.modal('show');

        var faculty = table.find('.faculty-' + res.id);
        faculty.find('.code').text(res.code);
        faculty.find('.name').text(res.name);
        return false;

    });
    // ---------------------show modal for updating
    $('body').on('click', 'btn-edit-major', function () {
        var modal = $('#editFaculty');
        var url = $(this).data('url');
        var code = $(this).closest('tr').find('.code').text();
        var name = $(this).closest('tr').find('.name').text();

        modal.find('form').attr('action', url);
        modal.find('input[name=code]').val(code);
        modal.find('input[name=name]').val(name);

        modal.modal('show');
    });
    $('form.frm-edit-faculty').submit(function () {
        console.log(999);
        var modal = $('#editFaculty');
        var form = modal.find('form');
        var table = $('.table-faculty');
        // $.ajax({
        //     url: form.attr('action'),
        //     method: form.attr('method'),
        //     data: form.serialize(),
        //     success: function (res) {
        //         console.log(modal);
        //         console.log(form);
        //         console.log(table);
        //         var faculty = table.find('.faculty-' + res.id);
        //         faculty.find('.code').text(res.code);
        //         faculty.find('.name').text(res.name);
        //
        //         modal.modal('hide');
        //     },
        //     error: function () {
        //         console.log('Loi!');
        //     }
        // });
        return false;
    });




    //--------------------------------------------------Control for Setting model
    $('body').on('click','.btn-edit-setting', function () {
       var modal = $('#edit-seting');
       var url = $(this).data('url');
       var _key = $(this).data('_key');
       var val = $(this).data('val');
       var des = $(this).data('des');

       modal.find('#form-edit').attr('action', url);
       modal.find('input[name=_key]').val(_key);
       modal.find('input[name=value]').val(val);
       modal.find('textarea').val(des);

       modal.modal('show');

    });
    // ----------------------------------------event edit "SETTING"
    $('#form-edit').submit(function () {
        var modal = $('#edit-seting');
        var form = modal.find('#form-edit');
        var table = $('#tabl-setting');

        $.ajax({
            url: form.attr('action'),
            method: form.attr('method'),
            data: form.serialize(),
            success: function (res) {
                var tr = table.find('.tr-setting-' + res.id);

                tr.find('._key').text(res._key);
                tr.find('.val').text(res.val);
                tr.find('.des').text(res.des);
                // tr.find('.btn-edit-setting').data('')


                tr.find('button').data('_key',res._key);
                tr.find('button').data('val',res.val);
                tr.find('button').data('des',res.des);

                modal.modal('hide');
                modal.find('input[name=_key]').val(res._key);
                modal.find('input[name=value]').val(res.val);

                modal.find('textarea').val(res.des);
            },error: function (err) {
                console.log(err);
            }
        });
        return false;

    });



    // ----------------------------Control for Majors
    $('.frm-add-major').submit(function () {
        var modal = $('#addMajor');
        var form = modal.find('frm-add-major');
        var table = $('.table-major');
        $.ajax({
            url: form.attr('action'),
            method: form.attr('method'),
            data: form.serialize(),
            success: function (res) {
                console.log(res);

                modal.modal('hide');
            },
            error: function () {
                console.log('Loi js add major');
            }
        });

        return false;
    });
    $('body').on('click', '#btn-edit-major', function () {
        var modal = $('#modalEditMajor');
        var url = $(this).data('url');
        var code = $(this).data('code');
        var name = $(this).data('name');

        modal.find('form').attr('action', url);
        modal.find('input[name=code]').val(code);
        modal.find('input[name=name]').val(name);

        modal.modal('show');
    });

    $('#form-edit-major').submit(function() {
        let modal = $('#modalEditMajor');
        let table = $('.table-major');
        let form = $('.form-edit-major');

        $.ajax({
            url: form.attr('action'),
            method: form.attr('method'),
            data: form.serialize(),
            success: function (res) {
                var tr = table.find('.tr-major-' + res.id);

                tr.find('.td-major-code').text(res.code);
                tr.find('.td-major-name').text(res.name);

                tr.find('button').data('code',res.code);
                tr.find('button').data('name',res.name);

                modal.modal('hide');
                modal.find('input[name=code]').val(res.code);
                modal.find('input[name=name]').val(res.name);
            },
            error: function(err){
                console.log(err)
            }
        });
        return false;
    })

    //----------------------------Control for subjects
    $('body').on('click','.btn-edit-subject', function () {
        let modal = $('#modal-edit-subject');
        let url = $(this).data('url');
        let code = $(this).data('code');
        let name = $(this).data('name');
        let faculty_id = $(this).data('faculty-id');

        modal.find('form').attr('action',url);
        modal.find('input[name=code]').val(code);
        modal.find('input[name=name]').val(name);
        modal.find('.faculty_id').val(faculty_id);

        modal.modal('show');
    });
    $('#form-edit-subject').submit(function () {
        let modal = $('#modal-edit-subject');
        let table = $('.table-subject');
        let form = $('.form-edit-subject');

        $.ajax({
            url: form.attr('action'),
            method: form.attr('method'),
            data: form.serialize(),
            success: function (res) {
                var tr = table.find('.tr-subject-'+res.id);

                tr.find('.')
            },
            error: function (err) {
                console.log(err);
            }
        });
        return false;
    });

    $('table tbody tr td ').on('click','button.btn-danger',function(){
        $('#delete-modal').modal('show');
        let url = $(this).data('url');
        let del_modal = $('a.modal-accept').attr('href',url);
    });

// ----------------------- Side-bar click get table
    $('ul.treeview-menu').on('click','.sidebar-majors',function () {
        $.ajax({
            url: '/majors/loadList',
            type: 'GET',
            success: function (response) {
                $('div #loading-ajax ').html(response.html);
            }
        })
        return false;
    })
    $('ul .treeview-menu').on('click','.sidebar-faculties',function () {
        $.ajax({
            url: '/faculties/loadList',
            type: 'GET',
            success: function (response) {
                $('div #loading-ajax ').html(response.html);
            }
        });
    })
    $('ul .treeview-menu').on('click','.sidebar-students',function () {
        $.ajax({
            url: '/students/loadList',
            type: 'GET',
            success: function (response) {
                $('div #loading-ajax ').html(response.html);
            }
        });
    })
})