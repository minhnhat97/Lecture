<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Major extends Model
{
    // table majors
    protected $fillable = ['_token','code','name','faculty_id'];

    public function faculty()
    {
        return $this->belongsTo('\App\Faculty');
    }

    public function student()
    {
        return $this->hasMany('\App\Model\Student');
    }
}
