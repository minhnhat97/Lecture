<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    protected $table = 'faculties';

    protected $fillable = ['name','code'];

    /*public function link() // show how elelements relating
    {
        return $this->
    }*/


    public function majors()
    {
        return $this->hasMany('App\Model\Major', 'faculty_id', 'id');
    }

    public function students()
    {
        return $this->hasMany(Student::class, 'faculty_id', 'id');
    }

    public function students_not_major()
    {
        return $this->hasMany(Student::class, 'faculty_id', 'id')->whereNull('major_id');
    }
}
