<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
    protected $table = "lecture";
}
