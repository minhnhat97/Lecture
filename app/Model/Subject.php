<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = ['code','name','faculty_id'];

    public function faculty()
    {
        return $this->belongsTo('\App\Model\Faculty');
    }

}
