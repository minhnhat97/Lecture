<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = ['code','name','gender','image','dob',
                            'phone','address','major_id','subject_id'];


    public function major()
    {
        return $this->belongsTo('App\Model\Major');
    }
    public function faculty()
    {
        return $this->belongsTo('App\Model\Faculty', 'faculty_id', 'id');
    }

//    public function subject()
//    {
//        return $this->belongsToMany('App\Model\Subject');
//    }
}
