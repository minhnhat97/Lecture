<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
//    protected $fillable = ['_key'];

    public static function getByKeyValue()
    {
        $settings = Setting::all();
        if ($settings) {
            $tmp = [];
            foreach ($settings as $setting) {
                $tmp[$setting->_key] = $setting->value;
            }

            $settings = $tmp;
        }
        return $settings;
    }
}
