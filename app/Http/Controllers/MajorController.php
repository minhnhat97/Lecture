<?php

namespace App\Http\Controllers;

use App\Model\Major;
use App\Model\Faculty;
use App\Model\Setting;
use App\Model\Student;
use Illuminate\Http\Request;
use function PHPSTORM_META\elementType;

class MajorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $major_id_null = Student::whereNull('major_id')->get();
        $faculties = Faculty::get();
        $majors = Major::orderBy('id','desc')->paginate(3);
        return view('major.index',[
            'faculties'=>$faculties,
            'major_id_null'=>$major_id_null,
            'majors'=>$majors]);
    }

    /*public function loadList()
    {
        $majors = Major::orderBy('id','desc')->get();
        $faculties = Faculty::get();
        $html = view('major.list', ['majors' => $majors,'faculties'=>$faculties])->render();
        return response()->json(['html' => $html]);
    }*/

    public function add()
    {
        $faculties = Faculty::get();
        return view('major.add')->with('list',$faculties);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postAdd(Request $request)
    {
        $newMajor =
            [
                'code' => $request->code,
                'name' => $request->name,
                'faculty_id' => $request->faculty_id,
            ];
        Major::create($newMajor);
        return redirect(route('majors.index'))->with('alert','Add new major successfully');
    }

    public function auto_create($faculty_id)
    {
        $faculty = Faculty::findOrFail($faculty_id);
        $settings = Setting::getByKeyValue();
        $students = Student::where(['faculty_id' => $faculty_id, 'major_id' => null])->get();

        $std_ids = [];//This for storing id of students in a major
        $i = 0;

        //Check if number of students of a major satisfy that can create a new major
        //That means the number of students should be at least the min number of student
        //has been config in Model "Setting"
        $student_num_min = $settings['student_num_min'];

        if (count($students) >= $student_num_min )
            {
                if ($students) {
                    foreach ($students as $key => $student)
                    {
                        $std_ids[$key / (int)$settings['student_num_max']][] = $student->id;

                    }
                }

                foreach ($std_ids as $key => $std_id) {
                    if ((int)$settings['student_num_min'] > count($std_ids[$key])) {
                        {
                            $std_ids[$key-1] = array_merge($std_ids[$key-1], $std_ids[$key]);
                            unset($std_ids[$key]);
                            //delete the "nano class" after merging
                        }
                    }
                }

                foreach ($std_ids as $key)
                {
                    $major = new Major;
                    $major->code = $faculty->code .'.'. date('Y') .'000'. ($i+1);
                    $major->name = $faculty->code . ' ' . ($i+1);
                    $major->faculty_id = $faculty_id;
                    if ($major->save())
                    {
                        Student::whereIn('id', $std_ids[$i])->update(['major_id' => $major->id]);
                    }
                    $i++;
                }
                return redirect(route('majors.index'))->with('alert', 'Lớp được mở thành công');
            }
        return redirect(route('majors.index'))->with('error', 'Lớp không được mở vì số lượng sinh viên ít hơn so với quy định');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $major = Major::findOrFail($id);
        $major->code = $request->code;
        $major->name = $request->name;

        if ($major->save())
        {
            return response()->json([
                'id' => $major->id,
                'code' => $major->code,
                'name' => $major->name,
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Major::find($id)->delete();
        return redirect(route('majors.index'))->with('alert','Deleted successfully');
    }
}
