<?php


namespace App\Http\Controllers;

use App\Model\Faculty;
use App\Http\Requests\StoreFaculty;
use Illuminate\Support\Facades\Auth;
use DB;

class FacultyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('faculty.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        // Show how many classes does each faculty " has "
        return view('faculty.add');
    }


    public function store(StoreFaculty $request)
    {
        $newFac = $request->all();
        Faculty::create($newFac);
        return redirect('faculties/index')->with('alert', 'Created Faculty Successfully');
    }

    public function loadList()
    {
        $list = Faculty::orderBy('id', 'DESC')->get();
        $html = view('faculty.list', ['list' => $list])->render();
        return response()->json(['html' => $html]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Faculty = Faculty::findOrFail($id);
        return view('faculty.edit', ['data' => $Faculty]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreFaculty $request, $id)
    {
        $data = $request->all();
        $faculty = Faculty::findOrFail($id);
        $faculty->code = $data['code'];
        $faculty->name = $data['name'];

        if ($faculty->save()) {
            return response()->json([
                'id' => $faculty->id,
                'code' => $faculty->code,
                'name' => $faculty->name,
            ]);
        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $fac = Faculty::find($id);
        $fac->delete();
        return redirect('faculties/index')->with('alert', 'Deleted Faculty Successfully');
    }
}
