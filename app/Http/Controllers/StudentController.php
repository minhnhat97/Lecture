<?php

namespace App\Http\Controllers;

use App\Model\Faculty;
use App\Model\Major;
use App\Model\Setting;
use App\Model\Student;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use File;
use Session;

class StudentController extends Controller
{
    //Setting number of students each major has


    public function index()
    {
        /*        $query = Faculty::with('students');

        $query->whereHas('students', function ($q) {
            $q->whereNull('major_id');
        });*/
        /*if ($major_id_null) {
            $tmp = [];
            foreach ($major_id_null as $item) {
                $tmp[(int)$item->faculty->id] =$item;
            }

            $major_id_null = $tmp;
        }
        dd($major_id_null);*/
        $faculties = Faculty::with('students_not_major')->get();
        $num_majorIdNull = Student::whereNull('major_id')->count();
        $majors = Student::with('major')->orderBy('id', 'desc');
        $students = Student::orderBy('id', 'desc')->Paginate(5);
        $fac = Faculty::get();
        return view('student.index', [ 'list' => $majors,
                                            'faculties'=>$faculties,
                                            'num_majorIdNull'=>$num_majorIdNull,
                                            'students'=>$students,
                                            'listFacs'=>$fac,
                                            ]);
    }

    public function add()
    {
        $majors = Major::get();
        return view('student.add')->with('list', $majors);
    }

    public function import(Request $request)
    {
        $fac = Faculty::get();
        $students = Student::orderBy('id', 'desc')->Paginate(3);
        $this->validate($request, array('file' => 'required'));
        if ($request->hasFile('file'))
        {
            $extension = File::extension($request->file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv")
            {

                $path = $request->file->getRealPath();
                $data = Excel::selectSheetsByIndex(0)->load($path, function ($reader) {
                })->get()->toArray();
                $count_std = Student::count();
                if (!empty($data))
                {
                    foreach ($data as $key => $item)
                    {
                        $k = str_pad($count_std + $key +1,5,0,STR_PAD_LEFT);
                        $insert[$key] = [
                            'code' => date('Y').".".$k,
                            'name' => $item['name'],
                            'email' => $item['email'],
                            'phone' => $item['phone'],
                            'faculty_id' => $request->faculty_id
                        ];
                    }
                    if (!empty($insert))
                    {
                        $listFaculty = Faculty::all();
                        $insertData = DB::table('students')->insert($insert);
                        return redirect()->route('students.index');

                    }
                }
            }

            return back();

        } else {
            Session::flash('error', 'File is a ' . ' file.!! Please upload a valid xls/csv file..!!');
            return back();
        }
    }


        /*public function loadList()
        {
            $students = Student::orderBy('id', 'desc')->simplePaginate(3);
            $fac = Faculty::get();
            $html = view('student.list', ['students' => $students, 'listFacs' => $fac])->render();
            return response()->json(['html' => $html]);
        }*/


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function postAdd(Request $request)
    {

        if ($request->hasFile('image')) {
            $filename = $request->code . $request->image->getClientOriginalName();
            $path = $request->file('image')->storeAs('\public', $filename);
//
        }
        $data = [
            'code' => $request->code,
            'name' => $request->name,
            'gender' => $request->gender,
            'image' => $filename,
            'dob' => $request->dob,
            'phone' => $request->phone,
            'address' => $request->address,
            'major_id' => $request->major_id,
            'subject_id' => $request->subject_id,
        ];
        Student::create($data);
        return redirect(route('students.index'))->with('alert', 'Add student successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::find($id);
        $listMajor = Major::all();
        return view('student.edit', ['student' => $student, 'list' => $listMajor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $filename = Student::find($id)->image;
        if ($request->hasFile('image')) {
            $filename = $request->code . $request->image->getClientOriginalName();
            $request->file('image')->storeAs('\public', $filename);
        }
        $data = [
            'code' => $request->code,
            'name' => $request->name,
            'gender' => $request->gender,
            'image' => $filename,
            'dob' => $request->dob,
            'phone' => $request->phone,
            'address' => $request->address,
            'major_id' => $request->major_id,
            'subject_id' => $request->subject_id,
        ];
        Student::find($id)->update($data);
        return redirect(route('students.index'))->with('alert', 'Updated student successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Student::find($id)->delete();
        return redirect(route('students.index'))->with('alert', 'Delete student successfully');
    }


}
