<?php

namespace App\Http\Controllers;

use App\Model\Setting;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function list()
    {
        $settings = Setting::get();
        return view('settings.list')->with(['settings'=>$settings]);
    }

    public function add()
    {
        return view('settings.add');
    }

    public function store(Request $request)
    {
        $data_settings = [
            '_key' => $request->_key,
            'value' => $request->value,
            'description' => $request->description
        ];
        Setting::insert($data_settings);
        return redirect(route('settings.list'))->with('alert','Thêm thiết lập thành công');

    }

    public function update(Request $request, $id)
    {
        $data = \Request::all();
        $setting = Setting::findOrFail($id);
        $setting->_key = $request->_key;
        $setting->value = $request->value;
        $setting->description = $request->description;

        if($setting->save())
        {
            return response()->json([
                'id' => $setting->id,
                '_key' => $setting->_key,
                'val' => $setting->value,
                'des' => $setting->description
            ]);
        }
    }

    public function delete($id)
    {
        $set = Setting::findOrFail($id);
        $set->delete();
        return redirect(route('settings.list'))->with('alert', 'Xóa thành công');
    }
}
