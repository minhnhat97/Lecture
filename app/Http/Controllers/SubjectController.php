<?php

namespace App\Http\Controllers;
use App\Http\Requests\StoreFaculty;
use App\Model\Faculty;
use App\Model\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = Subject::orderBy('id','desc')->get();
        $listFacs = Faculty::all();
        return view('subject.index',['subjects'=>$subjects,'listFacs'=>$listFacs]);
    }

    public function loadList()
    {
        $subjects = Subject::orderBy('id','desc')->get();
        $html = view('subject.list',['subjects'=>$subjects])->render();
        return response()->json(['html'=>$html]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $data = Faculty::all();
        return view('subject.add',['list'=>$data]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newSubject = [
            'code' => $request->code,
            'name' => $request->name,
            'faculty_id' => $request->faculty_id
        ];

        Subject::create($newSubject);
        return redirect(route('subjects.index'))->with('alert','Thêm môn học thành công');
    }

    public function postAdd(StoreFaculty $request)
    {
        $data =
            [
                'code' => $request->code,
                'name' => $request->name,
                'faculty_id' => $request->faculty_id,
            ];
        Subject::create($data);
        return redirect( route('subjects.index'))->with('alert','Created Subject Successfully');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subject = Subject::findOrFail($id);
        $subject->code = $request->code;
        $subject->name = $request->name;
        $subject->faculty_id = $request->faculty_id;

        if($subject->save())
            return response()->json([
                'id' => $subject->id,
                'code' => $subject->code,
                'name' => $subject->name,
                'faculty_id' => $subject->faculty_id
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Subject::find($id)->delete();
        return redirect(route('subjects.index'))->with('alert','Deleted Subject Successfully');
    }
}
