<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFaculty extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
//    dd(9);

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|min:2',
            'name' => 'required|min:5'
        ];
    }

    public function messages()
    {
        return [
            'code.required' => 'Mã khoa cần được nhập',
            'code.min' => 'Độ dài mã khoa tối thiểu là 2 ký tự',
            'name.required' => 'Tên khoa cần được nhập',
            'name.min' => 'Độ dài tên khoa tối thiểu là 5 ký tự',
        ];
    }
}
