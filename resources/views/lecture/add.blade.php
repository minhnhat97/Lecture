@extends('welcome')
@section('add')
    <?php $open = 'major'?>
    <form action="{{route('majors.postAdd')}}" method="POST">
        @csrf
        <div class="input-group mt-2">
            <div class="input-group-prepend">
                <label for="inputEmail4" class="input-group-text">Faculty Name</label>
            </div>
            <select class="" name="faculty_id" id="">
            @foreach($list as $key)
                    <option value="{{ $key['id'] }}">{{ $key['name'] }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-row">
            <div class="form-group col-md-3">
                <label for="inputEmail4">Class's Code</label>
                <input type="text" name="code" class="form-control" id="inputEmail4" placeholder="CNTT3">
            </div>
            <div class="form-group col-md-5">
                <label for="inputEmail4">Class Name</label>
                <input type="text" name="name" class="form-control" id="inputEmail4" placeholder="Công nghệ thông tin 3">
            </div>
        </div>
        <a class="btn btn-outline-info" href="{{ route('majors.index') }}">Go back</a>
        <button type="submit" class="btn btn-primary">Add</button>
    </form>
@endsection