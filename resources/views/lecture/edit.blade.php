@extends('welcome')
@section('edit')
    <?php $open = 'major'?>
    <form action="/faculties/update/{{$data->id}}" method="POST">
        @csrf
        <div class="row container">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">Faculty's Code: </label>
                    <input value="{{$data->code}}" class="form-control" type="text" name="code">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Faculty Name: </label>
                    <input value="{{$data->name}}" class="form-control" type="text" name="name">
                </div>
            </div>
        </div>
        <a href="" class="btn btn-primary ml-3">Back to Homepage</a>
        <button type="submit" class="btn btn-info ml-3">Accept</button>
        </div>
    </form>
@endsection