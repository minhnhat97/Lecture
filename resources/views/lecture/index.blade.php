@extends('layouts.app')
@section('content')
    @if(session('alert'))
        <div class="alert alert-success">
            {{session('alert')}}
        </div>
    @endif
            <div class="table-load"></div>

{{--    @include('student.add')--}}

@endsection()
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            var table = $('.table-load');
            $.ajax({
                url: '{{ route('lecture.loadList') }}',
                type: "GET",
                success: function (res) {
                    table.html(res.html);
                },
                error: function () {
                    console.log('Loi!');
                }
            });

        });
    </script>
@endsection