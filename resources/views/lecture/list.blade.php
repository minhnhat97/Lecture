<div class="box-header">
    <a href="" class="btn btn-primary">
        <i class="fa fa-user-plus" aria-hidden="true"></i>
        Thêm giảng viên
    </a>
</div>
<table class="table table-striped" style="margin-left: 10px; margin-bottom: 0">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">MÃ GV</th>
        <th scope="col">TÊN GV</th>
        <th scope="col">GIỚI TÍNH</th>
        <th scope="col">LỚP</th>
        <th scope="col">HÌNH ẢNH</th>
        <th scope="col">SỐ ĐIỆN THOẠI</th>
        <th scope="col">ĐỊA CHỈ</th>
        <th scope="col" class="text-center">TÍNH NĂNG</th>
    </tr>
    </thead>
    <tbody>
    @foreach($lecturers as $key => $value)
        <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $value->code }}</td>
            <td>{{ $value->name }}</td>
            <td>{{ $value->gender }}</td>
            <td>{{ isset($value->major) ? $value->major->name : '' }}</td>
            <td>
                @if($value->image == null)
                    <img src="img/unknown.jpg" width="50px">
                    @else
                    <img width="80px" src="/storage/{{$value->image}}" alt="{{$value->name}}">
                @endif
            </td>
            <td>{{ $value->phone }}</td>
            <td>{{ $value->address }}</td>
            <td style="text-align: center">
                <a class="btn btn-yahoo" href="/students/edit/{{$value->id}}">
                    <i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                <a class="btn btn-danger" href="/students/delete/{{$value->id}}">
                    <i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>