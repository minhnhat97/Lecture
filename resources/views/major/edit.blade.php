<div class="modal fade" id="modalEditMajor" role="dialog" aria-labelledby="editFaculty">
    <div class="modal-dialog modal-warning modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3>Thay đổi thông tin Lớp</h3>
            </div>
            <div class="modal-body">
                <form class="form-edit-major" method="POST" >
                    @csrf
                    @if(count($errors) > 0)
                        <br>
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    <div class="form-group">
                        <label for="">Mã lớp: </label>
                        <input id="editcode" class=" form-control" type="text" name="code">
                    </div>
                    <div class="form-group">
                        <label for="">Tên lớp: </label>
                        <input id="editname" class="form-control" type="text" name="name">
                    </div>
                    <div style="float: right;margin-top: 25px">
                        <button type="button" class="btn btn-flickr" data-dismiss="modal">Hủy</button>
                        <button type="submit" class="btn btn-primary">Lưu thay đổi</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>