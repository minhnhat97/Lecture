    <div class="box-header">
        <button class="btn btn-primary" data-toggle="modal" data-target="#addMajor">
            <i class="fa fa-plus" aria-hidden="true"></i>
            Thêm lớp
        </button>
    </div>
    <table class="table table-major table-bordered table-striped" style="margin-left: 10px; margin-bottom: 0">
        <thead>
        <tr class="info">
            <th scope="col">#</th>
            <th scope="col">MÃ LỚP</th>
            <th scope="col">TÊN LỚP</th>
            <th scope="col">SỐ SV</th>
            <th scope="col" class="text-center">TÍNH NĂNG</th>
        </tr>
        </thead>
        <tbody>
        @foreach($majors as $key => $value)
            <tr class="tr-major-{{$value->id}}">
                <td>{{ $key+1 }}</td>
                <td class="td-major-code">{{ $value->code }}</td>
                <td class="td-major-name">{{ $value->name }}</td>
                <td>{{ $value->student->count() }}</td>
                <td style="text-align: center">
                    <button class="btn btn-yahoo" id="btn-edit-major" data-url="{{ route('majors.update', $value->id) }}"
                            data-code="{{ $value->code }}" data-name="{{ $value->name }}">
                        <i class="fa fa-pencil" aria-hidden="true"></i> Edit
                    </button>
                    <a href="{{route('majors.delete',$value->id)}}" class="btn btn-google btn-outline-danger">
                        <i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div style="float:right; padding-right: 3px">{{ $majors->links() }}</div>
    @include('major.add')
    @include('major.edit')
