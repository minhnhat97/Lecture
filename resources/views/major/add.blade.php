<div id="addMajor" class="modal fade" role="dialog">
    <div class="modal-dialog modal-primary modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>x</span>
                </button>
                <h3 class="modal-title">Thêm lớp mới</h3>
            </div>
            <div class="modal-body">
                <form class="frm-add-major" method="POST" action="{{route('majors.postAdd')}}">
                    @csrf
                    <div class="input-group mt-2">
                        <div class=" input-group-prepend">
                            <label for="inputEmail4" class="input-group-text">Faculty Name</label>
                        </div>
                        <select class="form-control" name="faculty_id" id="">
                            @foreach($faculties as $key)
                                <option value="{{ $key['id'] }}">{{ $key['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3" style="padding-left: 0px">
                            <label for="inputEmail4">Class's Code</label>
                            <input type="text" name="code" class="form-control" id="inputEmail4" placeholder="CNTT3">
                        </div>
                        <div class="form-group col-md-5">
                            <label for="inputEmail4">Class Name</label>
                            <input type="text" name="name" class="form-control" id="inputEmail4" placeholder="Công nghệ thông tin 3">
                        </div>
                    </div>
                    <br><br><br>
                    <div style="float: right;margin-top: 25px">
                        <button class="btn btn-outline-info" data-dismiss="modal" >Hủy</button>
                        <button type="submit" class="btn btn-primary">Tạo lớp</button>
                    </div>

                </form>
            </div>
            <div class="modal-footer">

            </div>
        </div>

    </div>
</div>