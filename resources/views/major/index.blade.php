@extends('layouts.app')
@section('content')
    @if(session('alert'))
        <div class="alert alert-success">
            {{session('alert')}}
        </div>
    @endif
    @if(session('error'))
        <div class="alert alert-danger">
            {{session('error')}}
        </div>
    @endif
    <div class="table-load">
        @include('major.list')
    </div>
@endsection()
{{--@section('scripts')--}}
    {{--<script type="text/javascript">--}}
        {{--$(document).ready(function () {--}}
            {{--var table = $('.table-load');--}}
            {{--$.ajax({--}}
                {{--url: '{{ route('majors.loadList') }}',--}}
                {{--type: "GET",--}}
                {{--success: function (res) {--}}
                    {{--table.html(res.html);--}}
                {{--},--}}
                {{--error: function () {--}}
                    {{--console.log('Loi!');--}}
                {{--}--}}
            {{--});--}}

        {{--});--}}
    {{--</script>--}}
{{--@endsection--}}
