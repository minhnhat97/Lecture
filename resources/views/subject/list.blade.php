    <div class="box-header">
        <button class="btn btn-info" data-target="#modal-add-subject" data-toggle="modal">
            <i class="fa fa-plus" aria-hidden="true"></i>
            Thêm môn học
        </button>
    </div>
        <table class="table table-subject table-striped" style="margin-left: 10px; margin-bottom: 0">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">MÃ MÔN</th>
                <th scope="col">TÊN MÔN</th>
                <th scope="col">KHOA</th>
                <th scope="col" class="text-center">TÍNH NĂNG</th>
            </tr>
            </thead>
            <tbody>
            @foreach($subjects as $key => $value)
                <tr class="tr-subject-{{$value->id}}">
                    <td class="td-">{{ $key+1 }}</td>
                    <td>{{ $value->code }}</td>
                    <td>{{ $value->name }}</td>
                    <td>{{ $value->faculty->name}}</td>
                    <td style="text-align: center">
                        <button class="btn btn-yahoo btn-edit-subject" data-code="{{$value->code}}"
                                data-name="{{$value->name}}" data-faculty-id="{{$value->faculty->id}}"
                                data-url="{{route('subjects.update',$value->id)}}">
                            <i class="fa fa-pencil" aria-hidden="true"></i> Edit</button>
                        <a class="btn btn-google" href="/subjects/delete/{{$value->id}}">
                            <i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
