@extends('layouts.app')
@section('content')
    @if(session('alert'))
        <div class="alert alert-success">
            {{session('alert')}}
        </div>
    @endif
    @if(session('error'))
        <div class="alert alert-danger">
            {{session('error')}}
        </div>
    @endif
    <div class="table-load">
        @include('subject.list')
        @include('subject.add')
        @include('subject.edit')
    </div>
@endsection()