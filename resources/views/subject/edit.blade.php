<div id="modal-edit-subject" class="modal fade" role="dialog">
    <div class="modal-dialog modal-primary modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>x</span>
                </button>
                <h3 class="modal-title">Bạn muốn sửa thông tin môn? </h3>
            </div>
            <div class="modal-body">
                <form id="form-edit-subject" action="{{route('subjects.store')}}" method="POST">
                    @csrf
                    <div class="input-group mt-2">
                        <div class="input-group-prepend">
                            <label for="inputEmail4" class="input-group-text">Faculty Name</label>
                        </div>
                        <select class="form-control faculty_id" name="faculty_id" id="">
                            @foreach($listFacs as $key)
                                <option class="" value="{{ $key['id'] }}">{{ $key['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-row">
                        <div class="form-group">
                            <label for="inputEmail4">Subject's Code</label>
                            <input type="text" name="code" class="form-control" id="inputEmail4" placeholder="CNTT3">
                        </div>
                        <div class="form-group">
                            <label for="inputEmail4">Subject Name</label>
                            <input type="text" name="name" class="form-control" id="inputEmail4" placeholder="Công nghệ thông tin 3">
                        </div>
                    </div>
                    <a class="btn alert-warning" data-dismiss="modal" >Hủy</a>
                    <button type="submit" class="btn btn-primary">Đồng ý</button>
                </form>
            </div>
            <div class="modal-footer">
            </div>
        </div>

    </div>
</div>