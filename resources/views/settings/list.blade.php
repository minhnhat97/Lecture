@extends('layouts.app')
@section('content')
    <div class="box-header">
        <button class="btn btn-primary" data-toggle="modal" data-target="#set-up">
            <i class="fa fa-plus" aria-hidden="true"></i>
            Thêm cài đặt
        </button>
    </div>
    <table id="tabl-setting" class="table table-striped text-secondary table-bordered" style="margin-left: 10px; text-align: center !important; margin-bottom: 0">
        <thead>
            <tr >
                <th style="text-align: center">Stt</th>
                <th style="text-align: center">Key</th>
                <th style="text-align: center">Giá trị</th>
                <th style="text-align: center">Mô tả thêm</th>
                <th style="text-align: center">Chức năng</th>
            </tr>
        </thead>
        <tbody>
        @foreach($settings as $key => $setting)
            <tr class="tr-setting-{{$setting->id}}" >
                <td>{{$key+1}}</td>
                <td class="_key">{{$setting->_key}}</td>
                <td class="val">{{$setting->value}}</td>
                <td class="des">{{$setting->description}}</td>
                <td>
                    <button class="btn btn-yahoo btn-edit-setting"
                            data-url="{{route('settings.update',$setting->id)}}"
                            data-_key="{{$setting->_key}}" data-val="{{$setting->value}}"
                            data-des="{{$setting->description}}">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                        Edit
                    </button>
                    <a class="btn btn-danger" href="{{route('settings.delete',$setting->id)}}">
                        <i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    @include('settings.edit')
    @include('settings.add')
@endsection
