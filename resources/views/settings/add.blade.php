    <div id="set-up" class=" modal fade" tabindex="-1" aria-labelledby="exampleModalLabel" role="dialog">
        <div class="set-ups modal-sm" style="margin-left: 40%" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h3 class="modal-title">Thêm thiết lập mới</h3>
                </div>
                <div class="modal-body">
                        <form action="{{route('settings.store')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="exampleInputEmail1">_key</label>
                                <input type="text" name="_key" class="form-control" placeholder="student111">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Value</label>
                                <input type="text" name="value" class="form-control" placeholder="100">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Description</label>
                                <textarea class="form-control" name="description" rows="3"></textarea>
                            </div>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                            <button type="submit" class="btn btn-primary">Lưu cài đặt</button>
                        </form>
                </div>
                <div class="modal-footer">

                </div>
            </div>

        </div>
    </div>

