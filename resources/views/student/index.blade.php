@extends('layouts.app')
@section('content')
    @if($num_majorIdNull > 0)
        <div class="alert alert-warning">
            <ul>
                Có <u>{{$num_majorIdNull}}</u> sinh viên vẫn chưa có lớp, trong đó:
                @foreach($faculties as $fac)
                    @if($fac->students_not_major->count() <> 0)
                        <li>Khoa {{$fac->name}}: {{$fac->students_not_major->count()}} sinh viên ---> <a
                                    href="{{route('majors.auto_create',$fac->id)}}">Tạo lớp</a></li>
                    @endif
                @endforeach
                <br>Nhấn <u>"Tạo lớp"</u> để xếp lớp cho những sinh viên này.

            </ul>
        </div>
    @endif
    @if(session('alert'))
        <div class="alert alert-success">
            {{session('alert')}}
        </div>
    @endif
    @if(session('error'))
        <div class="alert alert-danger">
            {{session('error')}}
        </div>
    @endif
    <div class="table-load">
        <div class="box-header">
            <button class="btn btn-primary" data-toggle="modal" data-target="#addStudent">
                <i class="fa fa-plus" aria-hidden="true"></i>
                Thêm sinh viên
            </button>
            <button class="btn btn-success" data-toggle="modal" data-target="#addListStudent">
                <i class="fa fa-plus" aria-hidden="true"></i>
                Thêm danh sách sinh viên
            </button>
            <div class="modal fade" role="dialog" id="addListStudent">
                <div class="modal-dialog modal-primary" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span>x</span>
                            </button>
                            <h3>Thêm danh sách sinh viên</h3>
                        </div>
                        <div class="modal-body">
                            <form action="{{ route('students.import') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group" style="margin-top: 10px">
                                    <label for="">Nhập khoa </label>
                                    <select class="form-control" name="faculty_id">
                                        @foreach($listFacs as $faculty)
                                            <option value="{{$faculty['id']}}">{{$faculty['name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                Choose your xls/csv File : <input type="file" name="file" class="form-control">
                                <input type="submit" class="btn btn-primary btn-lg"
                                       style="float:right;margin-top: 25px">
                            </form>
                        </div>
                        <div class="modal-footer">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-striped" style="margin-left: 10px; margin-bottom: 0">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">MÃ SV</th>
                <th scope="col">TÊN SV</th>
                <th scope="col">GIỚI TÍNH</th>
                <th scope="col">LỚP</th>
                <th scope="col">HÌNH ẢNH</th>
                <th scope="col">SỐ ĐIỆN THOẠI</th>
                <th scope="col">ĐỊA CHỈ</th>
                <th scope="col" class="text-center">TÍNH NĂNG</th>
            </tr>
            </thead>
            <tbody>
            @foreach($students as $key => $value)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $value->code }}</td>
                    <td>{{ $value->name }}</td>
                    <td>{{ $value->gender }}</td>
                    <td>{{ isset($value->major) ? $value->major->name : '' }}</td>
                    <td>
                        @if($value ->image == null)
                            <img src="img/unknown.jpg" width="50px">
                        @else
                            <img width="80px" src="/storage/{{$value->image}}" alt="{{$value->name}}">
                        @endif
                    </td>
                    <td>{{ $value->phone }}</td>
                    <td>{{ $value->address }}</td>
                    <td style="text-align: center">
                        <a class="btn btn-yahoo" href="/students/edit/{{$value->id}}">
                            <i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                        <button class="btn btn-danger delete-modal"
                                data-url="/students/delete/{{$value->id}}"
                                >
                            <i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div style="float: right;">{{ $students->links() }}</div>

@include('student.delete')
@endsection()