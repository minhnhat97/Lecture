@extends('layouts.app')
@section('add')
    <div class="">
        <form action="{{route('students.postAdd')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="input-group mt-2">
                <div class="input-group-prepend">
                    <label for="inputEmail4" class="input-group-text">Major</label>
                </div>
                <select class="" name="major_id" id="">
                    @foreach($list as $key)
                        <option value="{{ $key['id'] }}">{{ $key['name'] }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-row">
                <div class="form-group col-md-2">
                    <label for="inputEmail4">Student's Code</label>
                    <input type="text" name="code" class="form-control" id="inputEmail4" placeholder="CNTT3">
                </div>
                <div class="form-group col-md-3">
                    <label for="inputEmail4">Student Name</label>
                    <input type="text" name="name" class="form-control" id="inputEmail4" placeholder="Công nghệ thông tin 3">
                </div>
                <div class="form-group col-md-2">
                    <label for="inputEmail4">Gender</label>
                    <input type="text" name="gender" class="form-control" id="inputEmail4" placeholder="nam">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-3">
                    <label for="">Image</label> <br>
                    <input type="file" name="image" class="">
                </div>
                <div class="form-group col-md-2">
                    <label for="">Ngày sinh</label>
                    <input type="date" name="dob" class="form-control">
                </div>
                <div class="form-group col-md-2">
                    <label for="">Phone</label>
                    <input type="tel" name="phone" class="form-control">
                </div>
            </div>
            <div class="form-row col-md-7" style="margin-left: -15px;padding-right:0px;margin-right: 0px">
                <label for="">Địa chỉ</label>
                <textarea name="address" class="form-control"></textarea>
            </div>
            <br>
            <div class="form-row">
                <a class="btn btn-outline-info mr-2" href="{{ route('majors.index') }}">Go back</a>
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </form>
    </div>
@endsection