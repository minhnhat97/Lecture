<div class="modal fade" id="delete-modal" role="dialog">
    <div class="modal-dialog modal-warning modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                Bạn chắc muốn xóa dữ liệu sinh viên này ?
                <br><small>Nhấn <b><u>OK</u></b> để xóa</small>
            </div>
            <div class="modal-footer">
                <button class="btn btn-flickr" data-dismiss="modal"> Hủy </button>
                <a href="" class="btn btn-success modal-accept"> OK </a>
            </div>
        </div>
    </div>
</div>