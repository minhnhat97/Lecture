<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pie-chart"></i>
                    <span>Quản lý</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/faculties"><i class="fa fa-circle-o"></i> Khoa</a></li>
                    <li><a href="/majors"><i class="fa fa-circle-o"></i> Lớp</a></li>
                    <li><a href="/subjects"><i class="fa fa-circle-o"></i> Môn học</a></li>
                    <li><a href="/lectures"><i class="fa fa-circle-o"></i> Giảng viên</a></li>
                    <li><a href="/students"><i class="fa fa-circle-o"></i> Sinh viên</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{route('settings.list')}}">
                    <i class="fa fa-cogs" style="margin-right: 6px" aria-hidden="true"></i>
                    <span>Cài đặt</span>
                </a>

            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>