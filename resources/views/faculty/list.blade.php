<div class="box-header">
    <button class="btn btn-primary" data-toggle="modal" data-target="#addFaculty">
        <i class="fa fa-plus" aria-hidden="true"></i>
        Thêm khoa
    </button>
</div>
<table class="table table-bordered  table-striped table-faculty" style="margin-left: 10px; margin-bottom: 0">
    <thead>
    <tr class="info">
        <th class="text-center" scope="col">#</th>
        <th class="text-center" scope="col">MÃ KHOA</th>
        <th class="text-center" scope="col">TÊN KHOA</th>
        <th class="text-center" scope="col">SỐ LỚP</th>
        <th class="text-center" scope="col">TÍNH NĂNG</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $key => $value)
        <tr id="deleteTr{{$value->id}}" class="faculty-{{ $value->id }}">
            <td class="text-center">{{ $key+1 }}</td>
            <td class="text-center code">{{ $value->code }}</td>
            <td class="text-center name">{{ $value->name }}</td>
            <td class="text-center">{{ count($value->majors) }}</td>
            <td style="text-align: center">
                <button class="btn btn-yahoo btn-edit" data-url="{{ route('faculties.update', $value->id) }}"
                        data-code="{{ $value->code }}" data-name="{{ $value->name }}">
                    <i class="fa fa-pencil" aria-hidden="true"></i> Edit
                </button>
                <a class="btn btn-google btn-delete-item" href="{{route('faculties.delete',$value->id)}}">
                    <i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

@include('faculty.add')
@include('faculty.edit')
