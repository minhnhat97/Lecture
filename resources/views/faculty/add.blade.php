<div class="modal fade" id="addFaculty" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-info modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title" id="exampleModalLabel">Thêm khoa mới</h3>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('faculties.store') }}" class="frm-add-faculty">
                    @csrf
                    @if(count($errors) > 0)
                        <br>
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    <div class="form-group">
                        <label for="">Faculty's Code: </label>
                        <input class=" form-control" type="text" name="code">
                    </div>
                    <div class="form-group">
                        <label for="">Faculty Name: </label>
                        <input class=" form-control" type="text" name="name">
                    </div>
                    <div style="float: right;margin-top: 25px">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                        <button type="submit" class="btn btn-primary">Tạo khoa</button>
                    </div>
                </form>
            </div>

            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>