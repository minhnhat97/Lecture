<?php
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate;
use App\Http\Controllers\Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Login
Route::get('/', 'HomeController@index')->name('home');

Route::get('/login', 'HomeController@login')->name('login');
Route::post('/doLogin', 'HomeController@doLogin')->name('doLogin');

Route::get('/logout', 'HomeController@logout')->name('logout');
//Route::group(['middleware' => 'auth'], function () {
//    Route::post('/logout', 'HomeController@logout')->name('logout');
//
//});
//Logout
//
//Route::get('/', function () {
//    return redirect()->route('home');
//});

//Route::resource('/faculties', 'FacultyController');

Route::prefix('faculties')->middleware('auth')->group( function (){
    Route::get('/',function (){
        return redirect()->route('faculties.index');
    });
    Route::get('index','FacultyController@index')->name('faculties.index');
    Route::get('add','FacultyController@add');
    Route::post('store','FacultyController@store')->name('faculties.store');
    Route::get('edit/{id}','FacultyController@edit');
    Route::post('update/{id}','FacultyController@update')->name('faculties.update');
    Route::get('delete/{id}','FacultyController@delete')->name('faculties.delete','id');
    Route::get('loadList','FacultyController@loadList')->name('faculties.loadList');
});

Route::prefix('majors')->middleware('auth')->group( function (){
    Route::get('/','MajorController@index');
    Route::get('index','MajorController@index')->name('majors.index');
    Route::get('add','MajorController@add');
    Route::post('postAdd','MajorController@postAdd')->name('majors.postAdd');
    Route::get('edit/{id}','MajorController@edit');
    Route::post('update/{id}','MajorController@update')->name('majors.update');
    Route::get('auto_create/{faculty_id}','MajorController@auto_create')
                                    ->name('majors.auto_create','faculty_id');
    Route::get('delete/{id}','MajorController@destroy')->name('majors.delete','id');
});

Route::prefix('subjects')->middleware('auth')->group( function (){
    Route::get('/','SubjectController@index');
    Route::get('index','SubjectController@index')->name('subjects.index');
    Route::get('loadList','SubjectController@loadList')->name('subjects.loadList');
    Route::post('import','SubjectController@import')->name('subjects.import');
    Route::get('add','SubjectController@add');
    Route::post('store','SubjectController@store')->name('subjects.store');
    Route::get('import','SubjectController@import')->name('subjects.import');
    Route::get('edit/{id}','SubjectController@edit');
    Route::post('update/{id}','SubjectController@update')->name('subjects.update');
    Route::get('delete/{id}','SubjectController@destroy');
});

Route::prefix('students')->middleware('auth')->group( function (){
    Route::get('/','StudentController@index');
    Route::get('index','StudentController@index')->name('students.index');
//    Route::get('loadList','StudentController@loadList')->name('students.loadList');
    Route::post('import','StudentController@import')->name('students.import');
    Route::get('add','StudentController@add');
    Route::post('postAdd','StudentController@postAdd')->name('students.postAdd');
    Route::get('import','StudentController@import')->name('students.import');
    Route::get('edit/{id}','StudentController@edit');
    Route::post('update/{id}','StudentController@update');
    Route::get('delete/{id}','StudentController@destroy')->name('students.delete','id');
});
Route::prefix('lectures')->middleware('auth')->group( function (){
    Route::get('/','LectureController@index');
    Route::get('index','LectureController@index')->name('lecture.index');
    Route::get('loadList','LectureController@loadList')->name('lecture.loadList');
    Route::post('import','LectureController@import')->name('lecture.import');
    Route::get('add','LectureController@add');
    Route::post('postAdd','LectureController@postAdd')->name('lecture.postAdd');
    Route::get('import','LectureController@import')->name('lecture.import');
    Route::get('edit/{id}','LectureController@edit');
    Route::post('update/{id}','LectureController@update');
    Route::get('delete/{id}','LectureController@destroy');
});
Route::prefix('settings')->middleware('auth')->group( function (){
    Route::get('/','SettingController@list');
    Route::get('list','SettingController@list')->name('settings.list');
    Route::get('loadList','SettingController@loadList')->name('settings.loadList');
    Route::post('import','SettingController@import')->name('settings.import');
    Route::get('add','SettingController@add');
    Route::post('store','SettingController@store')->name('settings.store');
    Route::get('import','SettingController@import')->name('settings.import');
    Route::post('update/{id}','SettingController@update')->name('settings.update');
    Route::get('delete/{id}','SettingController@delete')->name('settings.delete','id');
});

//Auth::routes();

