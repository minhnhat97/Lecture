<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('classes')->insert(
            [
                'code'=>'CNTT3',
                'name'=>'Công nghệ thông tin 3',
                'faculty_id'=>'1'
            ]);
    }
}
