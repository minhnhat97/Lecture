<?php

use Illuminate\Database\Seeder;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subjects')->insert(
            [
                'code'=>'TCC',
                'name'=>'Toán cao cấp 2A',
                'faculty_id'=>'1'
            ]);
    }
}
